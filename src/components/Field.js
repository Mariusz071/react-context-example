import React, { Component } from "react"
import LanguageContext from "../contexts/LanguageContext"
import dictionary from "../dictionary.json"

class Field extends Component {
  static contextType = LanguageContext

  render() {
    const text = dictionary[this.context].field
    return (
      <div className="ui field">
        <label>{text}</label>
        <input />
      </div>
    )
  }
}

export default Field
