import React, { Component } from "react"

import UserForm from "./UserForm"
import LanguageContext from "../contexts/LanguageContext"
import ColorContext from "../contexts/ColorContext"

class App extends Component {
  state = { language: "english" }

  changeLanguage = language => {
    this.setState({ language })
  }
  render() {
    console.log(ColorContext)
    const { language } = this.state
    return (
      <div className="ui container">
        <div>
          Select a language:
          <i
            className="flag us"
            onClick={() => this.changeLanguage("english")}
          />
          <i
            className="flag pl"
            onClick={() => this.changeLanguage("polish")}
          />
          <i className="flag nl" onClick={() => this.changeLanguage("dutch")} />
          <i className="flag cz" onClick={() => this.changeLanguage("czech")} />
        </div>
        <ColorContext.Provider value="red">
          <LanguageContext.Provider value={language}>
            <UserForm />
          </LanguageContext.Provider>
        </ColorContext.Provider>
      </div>
    )
  }
}

export default App
