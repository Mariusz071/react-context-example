import React, { Component } from "react"
import LanguageContext from "../contexts/LanguageContext"
import ColorContext from "../contexts/ColorContext"

import dictionary from "../dictionary.json"

//this.context variant
// class Button extends Component {
//   static contextType = LanguageContext

//   render() {
//     const text = dictionary[this.context].button
//     return <button className="ui button primary">{text}</button>
//   }
// }

// export default Button

//consumer variant
class Button extends Component {
  render() {
    return (
      <ColorContext.Consumer>
        {color => (
          <button className={`ui button ${color}`}>
            <LanguageContext.Consumer>
              {value => dictionary[value].button}
            </LanguageContext.Consumer>
          </button>
        )}
      </ColorContext.Consumer>
    )
  }
}

export default Button
